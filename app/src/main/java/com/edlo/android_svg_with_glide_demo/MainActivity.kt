package com.edlo.android_svg_with_glide_demo

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.ImageView
import com.edlo.utils.loadSvgIntoImageView

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val imgView = findViewById<ImageView>(R.id.imageView)
        val svgUrlString = "https://www.demo.com/your_svg_file.svg"
        loadSvgIntoImageView(svgUrlString, imgView)
    }
}