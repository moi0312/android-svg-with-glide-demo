package com.edlo.utils

import android.content.Context
import android.graphics.Picture
import android.graphics.drawable.PictureDrawable
import android.widget.ImageView
import androidx.annotation.NonNull
import androidx.annotation.Nullable
import com.bumptech.glide.Glide
import com.bumptech.glide.Registry
import com.bumptech.glide.RequestBuilder
import com.bumptech.glide.annotation.GlideModule
import com.bumptech.glide.load.DataSource
import com.bumptech.glide.load.Options
import com.bumptech.glide.load.ResourceDecoder
import com.bumptech.glide.load.engine.GlideException
import com.bumptech.glide.load.engine.Resource
import com.bumptech.glide.load.resource.SimpleResource
import com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions.withCrossFade
import com.bumptech.glide.load.resource.transcode.ResourceTranscoder
import com.bumptech.glide.module.AppGlideModule
import com.bumptech.glide.module.LibraryGlideModule
import com.bumptech.glide.request.RequestListener
import com.bumptech.glide.request.target.ImageViewTarget
import com.bumptech.glide.request.target.Target
import com.caverock.androidsvg.SVG
import com.caverock.androidsvg.SVGParseException
import java.io.IOException
import java.io.InputStream


fun loadSvgIntoImageView(urlString: String, view: ImageView) {
    val requestBuilder: RequestBuilder<PictureDrawable> = Glide.with(view.context)
        .`as`(PictureDrawable::class.java)
//        .transition(withCrossFade())
        .listener(SvgSoftwareLayerSetter())
    requestBuilder.load(urlString).into(view)
}

/** Decodes an SVG internal representation from an {@link InputStream}. */
class SvgDecoder: ResourceDecoder<InputStream, SVG> {

    override fun handles(source: InputStream, options: Options): Boolean = true

    override fun decode(source: InputStream, width: Int, height: Int, options: Options): Resource<SVG>? {
        try {
            val svg = SVG.getFromInputStream(source)
            if (width != Target.SIZE_ORIGINAL) svg.documentWidth = width.toFloat()
            if (height != Target.SIZE_ORIGINAL) svg.documentHeight = height.toFloat()
            return SimpleResource<SVG>(svg)
        } catch (ex: SVGParseException) {
            throw IOException("Cannot load SVG from stream", ex)
        }
    }
}
/**
 * Convert the {@link SVG}'s internal representation to an Android-compatible one ({@link Picture}).
 */
class SvgDrawableTranscoder : ResourceTranscoder<SVG?, PictureDrawable?> {

    @Nullable
    override fun transcode(@NonNull toTranscode: Resource<SVG?>, @NonNull options: Options): Resource<PictureDrawable?>? {
        val svg = toTranscode.get()
        val picture: Picture = svg.renderToPicture()
        val drawable = PictureDrawable(picture)

        return SimpleResource<PictureDrawable>(drawable)
    }

}

/** Module for the SVG.  */
@GlideModule
//class SvgModule : AppGlideModule() {
class SvgModule : LibraryGlideModule() {
    override fun registerComponents(@NonNull context: Context, @NonNull glide: Glide, @NonNull registry: Registry
    ) {
        registry.register(SVG::class.java, PictureDrawable::class.java, SvgDrawableTranscoder())
            .append(InputStream::class.java, SVG::class.java, SvgDecoder())
    }

//    // Disable manifest parsing to avoid adding similar modules twice.
//    override fun isManifestParsingEnabled(): Boolean = false
}

/**
 * Listener which updates the [ImageView] to be software rendered, because [ ]/[Picture][android.graphics.Picture] can't render on a
 * hardware backed [Canvas][android.graphics.Canvas].
 */
class SvgSoftwareLayerSetter : RequestListener<PictureDrawable?> {
    override fun onLoadFailed(e: GlideException?, model: Any?, target: Target<PictureDrawable?>, isFirstResource: Boolean): Boolean {
        val view: ImageView = (target as ImageViewTarget<*>).getView()
        view.setLayerType(ImageView.LAYER_TYPE_NONE, null)
        return false
    }

    override fun onResourceReady(resource: PictureDrawable?, model: Any?, target: Target<PictureDrawable?>,
            dataSource: DataSource?, isFirstResource: Boolean): Boolean {
        val view: ImageView = (target as ImageViewTarget<*>).getView()
        view.setLayerType(ImageView.LAYER_TYPE_SOFTWARE, null)
        return false
    }
}